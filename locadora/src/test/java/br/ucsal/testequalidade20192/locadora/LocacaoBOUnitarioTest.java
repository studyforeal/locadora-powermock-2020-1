package br.ucsal.testequalidade20192.locadora;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocacaoBO.class, ClienteDAO.class,VeiculoDAO.class, LocacaoDAO.class})
public class LocacaoBOUnitarioTest {
	/**
	 * Verificar se ao locar um veículo disponível para um cliente cadastrado, um
	 * contrato de locação é inserido.
	 * 
	 * Método:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observação1: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observação2: lembre-se de que o método locarVeiculos é um método command.
	 * 
	 * @throws Exception
	 */

	public static Cliente clienteTest;
	public static Veiculo veiculoTest;
	public static List<String> placasTest;
	public static List<Veiculo> veiculosTest;
	public static Locacao locacaoTest;

	@BeforeClass
	public static void setup() {
		clienteTest = new Cliente("123456789", "Testevaldo", "921938323");
		veiculoTest = new Veiculo("OKP-9283",2020, new Modelo("Gol"), 400.00);
		veiculosTest = new ArrayList<>();
		veiculosTest.add(veiculoTest);
		locacaoTest = new Locacao(clienteTest,veiculosTest,new Date("17/05/2020"),2);
		placasTest = new ArrayList<>();
		placasTest.add(veiculoTest.getPlaca());
	}


	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		mockStatic(ClienteDAO.class);
		when(ClienteDAO.obterPorCpf(anyString())).thenReturn(clienteTest);
		mockStatic(VeiculoDAO.class);
		when(VeiculoDAO.obterPorPlaca(anyString())).thenReturn(veiculoTest);
		mockStatic(LocacaoDAO.class);
		whenNew(Locacao.class).withAnyArguments().thenReturn(locacaoTest);
		LocacaoBO.locarVeiculos("123456789", placasTest, new Date("17/05/2020"),2);

		//Vefificação de metodos
		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf(anyString());
		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca(anyString());
		verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacaoTest);
		verifyNoMoreInteractions(LocacaoBO.class);
	}
}
